#include "Planet.h"
#include <ctime>

Planet::Planet(double mass, PlanetType type)
    : _mass(mass), _orig_mass(mass), _prev_mass(mass), _type(type) {

    _ran = boost::shared_ptr<Randomiser>(new Randomiser(time(NULL)));
}

Planet::~Planet() {
}

void Planet::adjust(double sigma) {
    /* 
     * If the planet object is RV then the measured mass is Msini
     * so a minimum guess. The mass can be increased to match 
     *
     * If the planet is transiting then the inclination is set
     * and the mass cannot change
     * */

    _prev_mass = _mass;

    if (_type == RV) {
        double deviation = -1000;
        while (_mass + deviation < _orig_mass) {
            deviation = _ran->inRange(-3.*sigma, 3.*sigma);
        }

        _mass += deviation;
    }
}

void Planet::reset() {
    _mass = _prev_mass;
}

bool Planet::transiting() {
    return _type == TRANS;
}
