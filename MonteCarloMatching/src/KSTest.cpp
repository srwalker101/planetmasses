#include "KSTest.h"
#include <nr/nr3.h>
#include <nr/sort.h>
#include <nr/ksdist.h>
#include <nr/kstests.h>
#include <algorithm>

struct GetMass {
    double operator()(const Planet &p) {
        return p.mass();
    }
};

void KSTest(const vector<Planet> &dist1, const vector<Planet> &dist2, double &D, double &p) {
    /* Need to make vectors of doubles */
    vector<double> mdist1(dist1.size()), mdist2(dist2.size());

    std::transform(dist1.begin(), dist1.end(), mdist1.begin(), GetMass());
    std::transform(dist2.begin(), dist2.end(), mdist2.begin(), GetMass());

    kstwo(mdist1, mdist2, D, p);
}
