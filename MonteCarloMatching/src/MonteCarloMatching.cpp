#include "MonteCarloMatching.h"
#include <iostream>
#include <sqlitepp/sqlitepp.hpp>
#include "Database.h"
#include "Planet.h"
#include "KSTest.h"
#include <ctime>
#include "dblocation.h"


using namespace std;

typedef vector<Planet>::iterator pIter;


App::App() 
: _nIter(1<<12) {
    _db = pDatabase(new Database(DBLOCATION));
    _ran = pRandomiser(new Randomiser(time(NULL)));

    _rChance = 0.5;
    _sigma = 0.5;
}

App::~App() {
}


int App::run() {
    vector<Planet> allMasses = _db->allMasses();
    vector<Planet> transitingMasses = _db->transitingMasses();

    /* Calculate the initial KS and p */
    double Dinit, pinit, D, p;

    KSTest(allMasses, transitingMasses, Dinit, pinit);
    cout << "Initial K-S: " << Dinit << ", p: " << pinit << endl;

    D = Dinit;
    p = pinit;

    for (unsigned int ii=0; ii<_nIter; ++ii) {

        for (pIter i=allMasses.begin(); i!=allMasses.end(); ++i) {

            if (!i->transiting()) {

                /* Only adjust random ones */
                if ((2. * _ran->doub() - 1) < _rChance) {

                    i->adjust(_sigma);

                    KSTest(allMasses, transitingMasses, D, p);

                    if (D < Dinit) {
                        /* Change was good, update the persistant values */
                        Dinit = D;
                        pinit = p;
                        cout << "Update: " << D << ", p: " << p << endl;

                    } else {
                        /* Change was bad */
                        i->reset();
                    }
                }


            }


        }

        if (ii % 100 == 0) {
            cout << "Iteration: " << ii << " -> D: " << D << ", p: " << p << endl;
        }


    }

    cout << "Final result: " << D << ", p: " << p << endl;




    return 0;
}
