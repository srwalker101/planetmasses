#include "Database.h"
#include <sqlitepp/sqlitepp.hpp>
#include <iostream>
#include <fstream>

using namespace std;
using sqlitepp::into;

namespace  {

    bool file_exists(const string &filename) {
        ifstream infile(filename.c_str());
        return infile;
    }
}

Database::Database(const string &filename)
    : _filename(filename) {

    if (file_exists(_filename)) {
        _session = session(new sqlitepp::session(_filename));
    } else {
        throw runtime_error("Cannot find database file");
    }

}

Database::~Database() {
}

std::vector<Planet> Database::allMasses() {

    double mass;
    int transit;
    std::vector<Planet> returns;
    sqlitepp::statement st(*_session);
    st << "select mass, transit from planet "
        "join meta on planet.id = meta.id",
        into(mass), into(transit);

    while (st.exec()) {
        if (mass != 0) {
            switch (transit) {
                case 0:
                    returns.push_back(Planet(mass, RV));
                    break;
                case 1:
                    returns.push_back(Planet(mass, TRANS));
                    break;
                default:
                    throw runtime_error("Unknown planet type encountered");
            }
        }
    }

    return returns;
}


std::vector<Planet> Database::transitingMasses() {
    double mass;
    std::vector<Planet> returns;
    sqlitepp::statement st(*_session);
    st << "select mass from planet join meta on planet.id = meta.id "
        "where meta.transit = 1",
        into(mass);

    while (st.exec()) {
        if (mass != 0) {
            returns.push_back(Planet(mass, TRANS));
        }
    }

    return returns;
}



