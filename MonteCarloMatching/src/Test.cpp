#include <UnitTest++/UnitTest++.h>
#include "MonteCarloMatching.h"
#include "Database.h"
#include <vector>
#include "Planet.h"
#include "KSTest.h"
#include <ctime>
#include <stdexcept>
#include "dblocation.h"

using namespace std;


SUITE(Database) {
    struct DefaultFixture {
        DefaultFixture() {
            db = new Database(DBLOCATION);
        }

        ~DefaultFixture() {
            delete db;
        }

        Database *db;
    };

    TEST(BadDatabase) {

        CHECK_THROW(Database db("BadDatabaseName"), runtime_error);
    }


    TEST_FIXTURE(DefaultFixture, DefaultCreator) {
    }

    TEST_FIXTURE(DefaultFixture, AllMasses) {

        vector<Planet> allMasses = db->allMasses();
        CHECK(allMasses.size() > 0);


    }

    TEST_FIXTURE(DefaultFixture, TransitingMasses) {
        vector<Planet> transitingMasses = db->transitingMasses();
        CHECK(transitingMasses.size() > 0);

    }

    TEST_FIXTURE(DefaultFixture, DifferentResults) {
        vector<Planet> allMasses = db->allMasses();
        vector<Planet> transitingMasses = db->transitingMasses();
        
        CHECK(allMasses.size() != transitingMasses.size());
    }

    TEST_FIXTURE(DefaultFixture, InitKS) {
        vector<Planet> allMasses = db->allMasses();
        vector<Planet> transitingMasses = db->transitingMasses();

        double D, p;
        KSTest(allMasses, transitingMasses, D, p);

        CHECK_CLOSE(D, 0.14, 0.05);
        CHECK_CLOSE(p, 0.002, 0.001);


    }


}

SUITE(Planet) {

    struct TRANSPlanet {

        TRANSPlanet() {
            p = new Planet(0.1, TRANS);
        }

        ~TRANSPlanet() {
            delete p;
        }

        Planet *p;

    };

    struct RVPlanet {

        RVPlanet() {
            p = new Planet(0.1, RV);
        }

        ~RVPlanet() {
            delete p;
        }

        Planet *p;

    };

    TEST_FIXTURE(RVPlanet, RVAdjust)  {
        for (int i=0; i<100; ++i) {
            p->adjust(i);
        }

        CHECK(p->mass() >= 0.1);

    }

    TEST_FIXTURE(TRANSPlanet, NoMassAdjust)  {

        for (int i=0; i<100; ++i) {
            p->adjust(i);
        }

        CHECK_CLOSE(p->mass(), 0.1, 0.0001);

    }

}


SUITE(Randomiser) {
    struct DefaultFixture {
        DefaultFixture() {
            ran = new Randomiser(time(NULL));
        }

        ~DefaultFixture() {
            delete ran;
        }

        Randomiser *ran;
    };

    TEST_FIXTURE(DefaultFixture, RangeTest)  {

        const int nTests = 1<<15;
        const double max = 3;
        const double min = -3;

        for (int i=0; i<nTests; ++i) {

            double ranValue = ran->inRange(min, max);
            CHECK((ranValue >= min) && (ranValue <= max));


        }


    }
}

int main(int argc, char *argv[]) {
    UnitTest::RunAllTests();
}
