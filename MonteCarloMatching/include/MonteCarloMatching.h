#ifndef MONTECARLOMATCHING_H_
#define MONTECARLOMATCHING_H_

#include <boost/shared_ptr.hpp>
#include "Database.h"
#include "Randomiser.h"

typedef boost::shared_ptr<Database> pDatabase;
typedef boost::shared_ptr<Randomiser> pRandomiser;

class App {
    public:
        App();
        virtual ~App();
        int run();
    
    private:
        pDatabase _db;
        long _nIter;
        pRandomiser _ran;
        double _rChance;
        double _sigma;

};


#endif /* end of include guard: MONTECARLOMATCHING_H_ */

