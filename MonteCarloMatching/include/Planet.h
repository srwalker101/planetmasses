#ifndef PLANET_H_BQCLRCMJ
#define PLANET_H_BQCLRCMJ


#include <boost/shared_ptr.hpp>
#include "Randomiser.h"

enum PlanetType {
    TRANS,
    RV
};


class Planet {
    public:
        Planet(double mass, PlanetType type);
        ~Planet();

        double mass() const { return _mass; }
        void adjust(double sigma);
        void reset();
        bool transiting();

    private:
        double _mass;
        double _orig_mass;
        double _prev_mass;
        PlanetType _type;

        /* Random number generator */
        boost::shared_ptr<Randomiser> _ran;

};




#endif /* end of include guard: PLANET_H_BQCLRCMJ */

