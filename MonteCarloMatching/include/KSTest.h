#ifndef KSTEST_H_EHDPDMXG
#define KSTEST_H_EHDPDMXG

#include "Planet.h"
#include <vector>

void KSTest(const std::vector<Planet> &dist1, const std::vector<Planet> &dist2, double &D, double &p);



#endif /* end of include guard: KSTEST_H_EHDPDMXG */

