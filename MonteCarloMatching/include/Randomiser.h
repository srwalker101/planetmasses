#ifndef RANDOMISER_H_ACMBHSBR
#define RANDOMISER_H_ACMBHSBR


#include <nr/nr3.h>
#include <nr/ran.h>

class Randomiser : public Ranq1 {
    public:
        Randomiser(unsigned int j);

        double inRange(double min, double max);
};


#endif /* end of include guard: RANDOMISER_H_ACMBHSBR */


