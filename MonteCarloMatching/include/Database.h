#ifndef DATABASE_H_QYBC95ZA
#define DATABASE_H_QYBC95ZA

#include <string>
#include <memory>
#include <vector>
#include <boost/shared_ptr.hpp>
#include "Planet.h"

namespace sqlitepp {
    class session;
}

//typedef std::unique_ptr<sqlitepp::session> session;
typedef boost::shared_ptr<sqlitepp::session> session;

class Database {
    public:
        Database(const std::string &filename);
        ~Database();

        std::vector<Planet> allMasses();
        std::vector<Planet> transitingMasses();

    private:
        std::string _filename;
        session _session;
        //sqlitepp::session *_session;


};




#endif /* end of include guard: DATABASE_H_QYBC95ZA */

